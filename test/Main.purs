module Test.Main (main) where

import Prelude (bind, discard, (>>=))
import Data.Maybe (Maybe(..))
import Effect.Console (error, log)
import Web.HTML (window)
import Web.HTML.Window (document)
import Web.HTML.HTMLDocument (body)

-- import Three

main = do
    log "Welcome to Three.purs"
    mb <- window >>= document >>= body
    case mb of
      Nothing → error "Could not get <body>"
      Just b → log "Body"
