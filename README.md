# `Three.purs`: A PureScript Wrapper for `three.js`

This is a straightforward [`PureScript`][psc] wrapper for
[_three.js_][three] in the [`Aff`][psc-aff] monad. It has provisions
for animation callbacks as well.

[psc]: http://www.purescript.org/ `PureScript`
[three]: https://threejs.org/ three.js
[psc-aff]: https://pursuit.purescript.org/packages/purescript-aff/ `purescript-aff`
