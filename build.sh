#!/bin/bash

set -e

export PATH=$PWD/node_modules/.bin:/opt/npm-packages/bin:$PATH

readonly webpack_options=(
    --color
    --mode=development
    --debug
    --verbose
    --display-error-details
)

(cd site && ln -sf ../src/index.html)
webpack ${webpack_options[@]}
