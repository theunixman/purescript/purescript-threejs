'use strict';

var three = require("three");

exports._meshBasicMaterial = function(opts) {
    return new three.MeshBasicMaterial(opts);
}
