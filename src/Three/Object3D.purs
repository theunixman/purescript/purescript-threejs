module Three.Object3D (class Object3D, position) where

import Three.Types (oget)
import Data.Lens (Getter')
import Three.Vector (Vector3)

class Object3D o

position ∷ ∀ o. Object3D o ⇒ Getter' o Vector3
position = oget "position"
