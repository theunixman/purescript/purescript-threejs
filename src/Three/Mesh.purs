module Three.Mesh (Mesh, mesh) where

import Prelude (($))
import Effect.Uncurried (EffectFn2, runEffectFn2)
import Effect.Unsafe (unsafePerformEffect)
import Three.Geometry (Geometry)
import Three.Material (Material)
import Three.Scene

foreign import data Mesh ∷ Type
foreign import _mesh ∷ EffectFn2 Geometry Material Mesh

instance sceneElementMesh ∷ SceneElement Mesh

mesh ∷ Geometry → Material → Mesh
mesh g m = unsafePerformEffect $ runEffectFn2 _mesh g m
