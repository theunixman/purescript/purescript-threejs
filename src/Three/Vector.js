'use strict';

var three = require("three");

exports._newVector3 = function() {
    return new three.Vector3();
};
