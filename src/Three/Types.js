'use strict';

exports._get = function(a, v) {
    return v[a];
};

exports._set = function(a, v, n) {
    v[a] = n;
    return v;
};
