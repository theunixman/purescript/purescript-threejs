'use strict';

var three = require("three");

exports._perspectiveCamera = function(fov, aspect, near, far) {
    return new three.PerspectiveCamera(fov, aspect, near, far);
}
