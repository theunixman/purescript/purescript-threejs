module Three.Camera where

import Effect
import Effect.Uncurried (EffectFn4, runEffectFn4)
import Three.Scene (class SceneElement)
import Three.Object3D (class Object3D)

foreign import data Camera ∷ Type

instance cameraSceneElement ∷ SceneElement Camera
instance cameraObject3D ∷ Object3D Camera

foreign import _perspectiveCamera ∷
  EffectFn4 Number Number Number Number Camera

perspectiveCamera ∷ Number → Number → Number → Number → Effect Camera
perspectiveCamera fov aspect near far =
  runEffectFn4 _perspectiveCamera fov aspect near far
