module Three.Vector (Vector3, vector3, x, y, z) where

import Prelude (($))
import Effect (Effect)
import Effect.Unsafe (unsafePerformEffect)
import Data.Lens (Lens)
import Three.Types (oprop)

foreign import data Vector3 ∷ Type
foreign import _newVector3 ∷ Effect Vector3

x ∷ Lens Vector3 (Effect Vector3) Number Number
x = oprop "x"

y ∷ Lens Vector3 (Effect Vector3) Number Number
y = oprop "y"

z ∷ Lens Vector3 (Effect Vector3) Number Number
z = oprop "z"

vector3 ∷ Vector3
vector3 = unsafePerformEffect $ _newVector3
