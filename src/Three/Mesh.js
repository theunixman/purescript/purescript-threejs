'use strict';

var three = require("three");

exports._mesh = function(g, m) {
    return new three.Mesh(g, m);
}
