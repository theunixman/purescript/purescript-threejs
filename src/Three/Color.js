'use strict';

var three = require("three");

exports._newColor = function(r, g, b) {
    return new three.Color(r, g, b);
};

exports._getHex = function(c) {
    return c.getHex();
};

exports._setHex = function(n, c) {
    c.setHex(n);
    return c;
}
