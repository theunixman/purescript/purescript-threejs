module Three.Geometry (
  Geometry,
  boxGeometry,
  faces
  ) where

import Prelude (($))
import Effect.Uncurried (EffectFn3, runEffectFn3)
import Effect.Unsafe (unsafePerformEffect)
import Three.Face (Face3)
import Three.Types (oget)
import Data.Lens (Getter')

foreign import data Geometry ∷ Type
foreign import _boxGeometry ∷ EffectFn3 Number Number Number Geometry

boxGeometry ∷ Number → Number → Number → Geometry
boxGeometry w h d = unsafePerformEffect $ runEffectFn3 _boxGeometry w h d

faces ∷ Getter' Geometry (Array Face3)
faces = oget "faces"
