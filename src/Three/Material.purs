module Three.Material (
  Material,
  VertexColors(..),
  MaterialOptions(..),
  meshBasicMaterial
  )where

import Prelude (class Show, bind, pure, show, ($), (<>), (<<<))
import Effect.Uncurried (EffectFn1, runEffectFn1)
import Effect.Unsafe (unsafePerformEffect)
import Foreign (ForeignError(..), fail, Foreign)
import Foreign.Class (class Decode, class Encode, decode, encode)
import Data.Maybe (Maybe)
import Data.Generic.Rep (class Generic)
import Data.Generic.Rep.Show (genericShow)
import Foreign.Generic (defaultOptions, genericDecode, genericEncode)

foreign import data Material ∷ Type

-- | Defines whether vertex coloring is used for ‘vertexColors’.
data VertexColors =
  -- | No colors for the vertices.
  ColorsNo |
  -- | Use vertex colors.
  ColorsVertex |
  -- | Use face colors.
  ColorsFace

derive instance genericVertexColors ∷ Generic VertexColors _

instance showVertexColors ∷ Show VertexColors where
  show = genericShow

instance encodeVertexColors ∷ Encode VertexColors where
  encode = case _ of
    ColorsNo → encode 0
    ColorsFace → encode 1
    ColorsVertex → encode 2

instance decodeVertexColors ∷ Decode VertexColors where
  decode v = do
    c ← decode v
    case c of
      0 → pure ColorsNo
      1 → pure ColorsFace
      2 → pure ColorsVertex
      _ → fail $ ForeignError $ "Invalid VertexColors: " <> show c

data BlendingMode =
  NoBlending |
  NormalBlending |
  AdditiveBlending |
  SubtractiveBlending |
  MultiplyBlending |
  CustomBlending

data BlendingEquation =
  AddEquation |
  SubtractEquation |
  ReverseSubtractEquation |
  MinEquation |
  MaxEquation

newtype MaterialOptions = MaterialOptions {
  vertexColors ∷ Maybe VertexColors,
  overdraw ∷ Maybe Number
  }

derive instance genericMaterialOptions ∷ Generic MaterialOptions _

instance showMaterialOptions ∷ Show MaterialOptions where
  show = genericShow

instance decodeMaterialOptions :: Decode MaterialOptions where
  decode = genericDecode $ defaultOptions {unwrapSingleConstructors = true}
instance encodeMaterialOptions :: Encode MaterialOptions where
  encode = genericEncode $ defaultOptions {unwrapSingleConstructors = true}

foreign import _meshBasicMaterial ∷ EffectFn1 Foreign Material

meshBasicMaterial ∷ MaterialOptions → Material
meshBasicMaterial = unsafePerformEffect
                    <<< runEffectFn1 _meshBasicMaterial
                    <<< encode
