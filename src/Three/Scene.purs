module Three.Scene (
  class SceneElement,
  Scene,
  newScene,
  addToScene,
  background) where

import Effect (Effect)
import Effect.Uncurried (EffectFn2, runEffectFn2)
import Data.Lens (Lens)
import Three.Color (Color)
import Three.Types (oprop)

foreign import data Scene ∷ Type
foreign import _newScene ∷ Effect Scene

newScene ∷ Effect Scene
newScene = _newScene

class SceneElement e

foreign import _addToScene ∷ ∀ e. SceneElement e ⇒ EffectFn2 Scene e e

addToScene ∷ ∀ e. SceneElement e ⇒ Scene → e → Effect e
addToScene s e = runEffectFn2 _addToScene s e

background ∷ Lens Scene (Effect Scene) Color Color
background = oprop "background"
