"use strict";

var three = require("three");

exports._newScene = function() {
    return new three.Scene();
}

exports._addToScene = function() {
    return function(scene, e) {
        scene.add(e);
        return e;
    }
}
