module Three.Types (oget, oprop) where

import Effect (Effect)
import Data.Lens(Lens, lens, Getter', to)
import Data.Function.Uncurried (Fn2, runFn2)
import Effect.Uncurried(EffectFn3, runEffectFn3)

foreign import _get ∷ ∀ o a. Fn2 String o a
foreign import _set ∷ ∀ o a. EffectFn3 String o a o

oget ∷ ∀ o a. String → Getter' o a
oget a = to (\o → runFn2 _get a o)

oprop ∷ ∀ o a. String → Lens o (Effect o) a a
oprop a = lens
          (\o → runFn2 _get a o)
          (\o v → runEffectFn3 _set a o v)
