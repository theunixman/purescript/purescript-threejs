module Three.Color (Color, color, hex, r, g, b) where

import Prelude (($))
import Effect (Effect)
import Effect.Uncurried (EffectFn2, runEffectFn2, EffectFn3, runEffectFn3)
import Effect.Unsafe (unsafePerformEffect)
import Three.Types (oprop)
import Data.Lens (Lens, lens)

foreign import data Color ∷ Type
foreign import _newColor ∷ EffectFn3 Number Number Number Color

foreign import _getHex ∷ Color → Int
foreign import _setHex ∷ EffectFn2 Int Color Color

color ∷ Number → Number → Number → Color
color re gr bl = unsafePerformEffect $ runEffectFn3 _newColor re gr bl

r ∷ Lens Color (Effect Color) Number Number
r = oprop "r"

g ∷ Lens Color (Effect Color) Number Number
g = oprop "g"

b ∷ Lens Color (Effect Color) Number Number
b = oprop "b"

hex ∷ Lens Color (Effect Color) Int Int
hex = lens _getHex (\n c → runEffectFn2 _setHex c n)
