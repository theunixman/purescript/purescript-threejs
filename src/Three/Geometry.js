'use strict';

var three = require("three");

exports._boxGeometry = function(h, w, d) {
    return new three.BoxGeometry(h, w, d);

};
