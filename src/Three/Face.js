'use strict';

var three = require("three");

exports._face3 = function(a, b, c, n, l, m) {
    return new three.Face3(a, b, c, n, l, m);
};
