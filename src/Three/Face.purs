module Three.Face (Face3, face3, color) where

import Prelude (($))
import Effect (Effect)
import Effect.Uncurried (EffectFn6, runEffectFn6)
import Effect.Unsafe (unsafePerformEffect)
import Three.Color (Color)
import Three.Vector (Vector3)
import Three.Types (oprop)
import Data.Lens (Lens)

foreign import data Face3 ∷ Type
foreign import _face3 ∷ EffectFn6 Int Int Int Vector3 Color Int Face3

face3 ∷ Int → Int → Int → Vector3 → Color → Int → Face3
face3 a b c n l m = unsafePerformEffect $ runEffectFn6 _face3 a b c n l m

color ∷ Lens Face3 (Effect Face3) Color Color
color = oprop "color"
