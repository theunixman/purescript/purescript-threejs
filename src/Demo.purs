module Demo (demo) where

import Data.Int (toNumber, round)
import Prelude (
  bind, discard, Unit, unit, pure, map,
  (>>=), (/), ($),
  (<<<), (<$>), (#),
  (*)
  )
import Data.Maybe (Maybe(..))
import Effect (Effect)
import Effect.Console (error, log)
import Web.HTML (window)
import Web.HTML.Window (document, Window, innerHeight, innerWidth)
import Web.HTML.HTMLDocument (body)
import Data.Lens (lengthOf, over, sequenceOf_, set, traversed, (.~), (^.))
import Effect.Random (random)
import Data.Array ((..), drop, take)

import Three.Color as C
import Three.Face as F
import Three.Geometry (Geometry, boxGeometry, faces)
import Three.Scene (Scene, newScene, addToScene, background)
import Three.Camera (Camera, perspectiveCamera)
import Three.Object3D (position)
import Three.Vector (x, y)
import Three.Material (meshBasicMaterial, MaterialOptions(..), VertexColors(..))
import Three.Mesh (mesh)

camera ∷ Scene → Window → Effect Camera
camera sce win = do
  h ← innerHeight win
  w ← innerWidth win
  let r = toNumber h / toNumber w
  c ← perspectiveCamera 70.0 r 1.0 1000.0 >>= addToScene sce
  let p = c ^. position
  _ ← set x 500.0 p >>= set y 150.0
  pure c

setColor ∷ Int → F.Face3 → Effect Unit
setColor c f = do
  _ ← f ^. F.color # C.hex .~ c
  pure unit

geometry ∷ Effect Geometry
geometry = do
  let w = toNumber 0xffffff
  let box = boxGeometry 200.0 200.0 200.0
  let ixs =
        over traversed (\i → i * 2) (0..(lengthOf traversed $ box ^. faces))

  let sc is = do
        c ← round <<< (w * _) <$> random
        let fs = take 2 $ drop is $ box ^. faces
        sequenceOf_ traversed $ map (setColor c) fs

  sequenceOf_ traversed $ map sc ixs
  pure box

demo ∷ Effect Unit
demo = do
    log "Welcome to Three.purs"
    mb <- window >>= document >>= body
    case mb of
      Nothing → error "Could not get <body>"
      Just b → log "Body"
    sc ← newScene >>= set background (C.color 240.0 240.0 240.0)
    cm ← window >>= camera sc
    cube ← geometry
    let mats = meshBasicMaterial $ MaterialOptions {
          vertexColors: Just ColorsFace,
          overdraw: Just 0.5
          }
    let cmsh = mesh cube mats

    _ ← addToScene sc cmsh
    log "Camera and scene."
